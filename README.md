evince-synctex
==============

This is a very small python-based ViM plugin to enable bidirectional
synchronization between Evince and (G)Vim.

## Usage

In ViM, use `\ls` to do a "forward" search, that is sync Evince with the
current ViM cursor position. Evince should show a red rectangle around the
corresponding content. If not, it might be because Evince uses a different path
than ViM to refer to the PDF file (because of symlinks for instance). A good
way to mitigate that is to set your viewer command to `evince $(readlink -f
<pdf file>)` since this plugin will try its best to normalize by dereferencing
symlinks.

In Evince, use CTRL-Click to do a "backward" search, that is show the ViM
buffer responsible for the content clicked on, with the cursor on the correct
line (Evince doesn't send column information).

Since Evince sends dbus signals, and receiving them requires a compatible event
loop, the "backward" synchronisation doesn't work in the terminal. The forward
search works normally even from a non-gui ViM.

## Settings

The plugin redefines `<LocalLeader>ls` locally to the buffer to map to its
syncing method with Evince which is the function `Evince_ForwardSearchLaTeX()`.
If you want to prevent that you can set:
* `no_plugin_maps` (will probably disable all plugin mappings)
* `no_tex_maps` (will probably disable all mappings of TeX ftplugins)
* `no_evincesync_maps` (specific to this plugin)

When the user CTRL-clicks in Evince, evince-synctex will move the cursor to the
corresponding position in all windows showing the relevant buffer. If
`g:EvinceSync_center` is set to `yes`, `true`, `on` or `1`, then the scrolling
position will additionally be set so that the cursor is centered in those
windows (the command `zz` is called). This is the default if
`g:EvinceSync_center` is unset but you can set it to any other value to disable
the behavior.

Furthermore, `g:EvinceSync_showbuf` can be set to a ViM Ex command, where `{}`
will be replaced by the number of the buffer corresponding to the CTRL-Click in
Evince. If undefined, evince-synctex uses `sbuffer {}` which means that a
currently inactive buffer will trigger a window split unless `switchbuf` is not
empty.

Another sensible value can be `let g:EvinceSync_showbuf='buffer {}'` but you
can also set it to an empty value to disable the buffer switch.

## Dependencies

For this plugin to work, the following must be satisfied:

* ViM must have been compiled with Python (2 or 3) support. If both are compiled
  in, the plugin favors Python 3
* The [pydbus module](https://github.com/LEW21/pydbus) must be installed for the
  selected Python version.
* The Gio introspection data for the gi-repository must be
  installed. This is probably the case if Evince is present on the system.

## Installing with Vundle

Add the line
```
Plugin 'https://gitlab.com/frnchfrgg-latex/evince-synctex.git'
```
to your `.vimrc` (or similar), resource it (or restart ViM) and run `:VundleUpdate`.

## Installing manually

Just copy the `after/ftplugin/tex_evince.vim` file to
`~/.vim/after/ftplugin/tex_evince.vim`.

The `after` directory is needed to ensure other plugins like LatexSuite do not
overwrite the `\ls` shortcut.
