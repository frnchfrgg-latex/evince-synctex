" Vim plugin for synchronizing Vim and Evince with synctex
" ---------------
"  Needs: python 2 or 3 support in Vim, Gio (probably available
"         if Evince is installed), and pydbus.
"  Remarks: Synchronization from Vim to Evince works everytime
"           Synchronization from Evince to Vim requires the GLib event
"           loop which is only available with GVim, not in a terminal.
"
" Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
"
" This program is free software: you can redistribute it and/or modify
" it under the terms of the GNU General Public License as published by
" the Free Software Foundation, either version 3 of the License, or
" (at your option) any later version.
"
" This program is distributed in the hope that it will be useful,
" but WITHOUT ANY WARRANTY; without even the implied warranty of
" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
" GNU General Public License for more details.
"
" You should have received a copy of the GNU General Public License
" along with this program.  If not, see <http://www.gnu.org/licenses/>.

if !has("pythonx")
    finish
endif

if !exists("no_plugin_maps") && !exists("no_tex_maps") && !exists("no_evincesync_maps")
    nmap <buffer> <LocalLeader>ls :call Evince_ForwardSearchLaTeX()<CR>
endif

if exists("g:loaded_evincesync")
    finish
endif
let g:loaded_evincesync = 1

pythonx << END
import vim
import os, sys
from pydbus import SessionBus
from gi.repository import Gio

bus = SessionBus()

def handle_signal(sender, obj, iface, signal, info):
    uri, (line, *_), *_ = info
    filename = os.path.realpath( Gio.File.new_for_uri(uri).get_path() )
    try:
        bufn = next(n for (n,b) in enumerate(vim.buffers)
                   if os.path.realpath(b.name) == filename)
    except StopIteration:
        return
    try:
        command = vim.eval("g:EvinceSync_showbuf")
    except vim.error:
        command = "sbuffer {}"
    try:
        vim.command(command.format(bufn+1))
    except vim.error as e:
        print(e)
        sys.stderr.write("Error in g:EvinceSync_showbuf: "+str(e))
        return
    prev_w = vim.current.window
    try:
        do_zz = vim.eval("g:EvinceSync_center") in ("yes", "true", "1", "on")
    except vim.error:
        do_zz = True
    for w in vim.windows:
        if os.path.realpath(w.buffer.name) == filename:
            w.cursor = (line, 0)
            if do_zz:
                vim.current.window = w
                vim.command("normal zz")
    vim.current.window = prev_w
    vim.command("redraw!")

bus.subscribe(iface="org.gnome.evince.Window",
              signal="SyncSource",
              signal_fired = handle_signal)
END

function Evince_ForwardSearchLaTeX()
pythonx << END
try:
    main_tex = os.path.realpath(vim.eval("Tex_GetMainFileName()"))
except vim.error:
    main_tex = os.path.realpath(vim.current.buffer.name)
if main_tex.endswith(".tex"):
    main_pdf = main_tex[:-4] + ".pdf"
else:
    main_pdf = main_tex + ".pdf"
pdf_uri = Gio.File.new_for_path(main_pdf).get_uri()

try:
    daemon = bus.get("org.gnome.evince.Daemon")["org.gnome.evince.Daemon"]
    address = daemon.FindDocument(pdf_uri, False)
except:
    address = ""
if address:
    try:
        windows = bus.get(address, "/org/gnome/evince/Evince").GetWindowList()
        window = bus.get(address, windows[0])
    except:
        pass
    else:
        source_name = os.path.realpath(vim.current.buffer.name)
        l, c = vim.current.window.cursor
        cursor = (l, c+1)
        window.SyncView(source_name, cursor, 0)
END
endfunction
